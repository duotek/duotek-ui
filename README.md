# @duotek/ui

# Набор ui для nuxt проектов

## Установка

```bash
yarn add -E @duotek/ui
# or
npm i -SE @duotek/ui
```

### Добавьте файл в папку к плагинам my-project/plugins/duotek-ui.js

```javascript
import Vue from "vue";
import DuotekUI from "@duotek/ui";

export default ({app}) => {
  Vue.use(DuotekUI);
}
```

### Подключите файл в nuxt.config.js

```javascript
export default {
  plugins: [
    '~/plugins/duotek-ui',
  ],
}
```

### Подключите стили nuxt.config.js

```javascript
export default {
  css: [
    '@duotek/ui/assets/scss/style.scss', // Все стили библиотеки
    '@/assets/scss/style.scss', // Стили вашего проекта
    '@duotek/ui/assets/scss/generated.scss', // Не обязательные стили с хелперами
  ],
}
```

### Включите транспиляцию nuxt.config.js

```javascript
export default {
  build: {
    transpile: ['@duotek/ui', '@popperjs/core', 'autosize'],
  }
}
```

### Документация

[ui.duotek.ru](http://ui.duotek.ru/docs/install).
