import fse from 'fs-extra';
import path from 'path';
import chalk from 'chalk';
import {vueTemplate, indexJsTemplate, indexScssTemplate} from './templates/component';

const createFlagFile = async (componentName) => {
	const componentPath = path.resolve('./components', componentName);
	if (!componentName) {
		console.log(chalk.green(`SKIP: componentName is missing`));
		return;
	}
	if (!await fse.exists(componentPath)) {
		await Promise.all([
			fse.outputFile(`${componentPath}/${componentName}.vue`, vueTemplate(componentName)),
			fse.outputFile(`${componentPath}/${componentName}.scss`, ''),
			fse.outputFile(`${componentPath}/index.js`, indexJsTemplate(componentName)),
			fse.outputFile(`${componentPath}/index.scss`, indexScssTemplate(componentName)),
		]).then(() => {
			console.log(chalk.green(`SUCCESS: Component - ${chalk.blue(componentName)} created`))
		}).catch((error) => {
			console.log(error)
		})
	} else {
		console.log(chalk.red(`ERROR: Component - ${chalk.blue(componentName)} already exist!`));
		process.exit(1);
	}
}

export default createFlagFile;

