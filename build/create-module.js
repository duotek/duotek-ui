import fse from 'fs-extra';
import path from 'path';
import chalk from 'chalk';
import {indexJsTemplate, indexScssTemplate, moduleJsTemplate} from './templates/module';

const createModule = async (moduleName) => {
	const modulePath = path.resolve('./modules', moduleName);

	if (!moduleName) {
		console.log(chalk.green(`SKIP: moduleName is missing`));
		return;
	}

	if (!await fse.exists(modulePath)) {
		await Promise.all([
			fse.outputFile(`${modulePath}/${moduleName}.js`, moduleJsTemplate(moduleName)),
			fse.outputFile(`${modulePath}/${moduleName}.scss`, ''),
			fse.outputFile(`${modulePath}/index.js`, indexJsTemplate(moduleName)),
			fse.outputFile(`${modulePath}/index.scss`, indexScssTemplate(moduleName)),
		]).then(() => {
			console.log(chalk.green(`SUCCESS: Module - ${chalk.blue(moduleName)} created`))
		}).catch((error) => {
			console.log(error)
		})
	} else {
		console.log(chalk.red(`ERROR: Component - ${chalk.blue(modulePath)} already exist!`));
		process.exit(1);
	}

}

export default createModule;

