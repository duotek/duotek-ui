import fs from 'fs';
import fse from 'fs-extra';
import chalk from 'chalk';
import path from 'path';

const createReExport = async (dirPath = '/', fileName = '', templateFn = () => {}) => {
	const blackList = ['index', 'DS_Store'];
	const output = path.resolve(dirPath, fileName);
	const files = fs.readdirSync(dirPath, { withFileTypes: true });
	const name = Array.from(files).filter((file) => {
		let taboo = blackList.filter((black) => {
			return file.name.includes(black);
		})
		return !taboo.length;
	}).map(dir => {
		return dir.name.replace('.js', '');
	});

	await fse.outputFile(output, templateFn(name)).then(() => {
		console.log(chalk.green(`SUCCESS: export file generated`))
	}).catch(() => {
		console.log(chalk.red(`ERROR: export file error!`));
		process.exit(1);
	});
}

export default createReExport;
