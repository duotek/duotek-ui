import sass from 'sass';
import path from 'path';
import fse from 'fs-extra';
import chalk from 'chalk';


const scssBootstrapFile = path.resolve(__dirname, '../assets/scss/bootstrap.scss');
const scssOutputFile = path.resolve(__dirname, '../assets/scss/_var-helper.scss');
const sassRender = sass.renderSync({file: scssBootstrapFile});

fse.outputFile(scssOutputFile, sassRender.css).then(() => {
	console.log(chalk.green(`SUCCESS: scss file generated`))
}).catch(() => {
	console.log(chalk.red(`ERROR: export file error!`));
	process.exit(1);
})
