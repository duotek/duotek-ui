import path from 'path';
import createComponent from './create-component';
import createReExport from './create-re-export';
import reExportTemplateJs from './templates/re-export-js';
import reExportTemplateScss from './templates/re-export-scss';

const arg = process.argv[2];
const componentName = arg ? 'D' + arg.slice(0, 1).toUpperCase() + arg.slice(1) : '';

const run = async () => {
	await createComponent(componentName);
	const dirPath = path.resolve('./components');

	await Promise.all([
		createReExport(dirPath, 'index.js', reExportTemplateJs),
		createReExport(dirPath, 'index.scss', reExportTemplateScss),
	]);
}

run().then(() => {
	process.exit(0);
});
