import createModule from './create-module';
import createReExport from './create-re-export';
import reExportTemplateJs from './templates/re-export-js';
import path from 'path';
import reExportTemplateScss from './templates/re-export-scss';

const arg = process.argv[2];
const moduleName = arg ? 'D' + arg.slice(0, 1).toUpperCase() + arg.slice(1) : '';

const run = async () => {
	await createModule(moduleName);
	const dirPath = path.resolve('./modules');
	await Promise.all([
		createReExport(dirPath, 'index.js', reExportTemplateJs),
		createReExport(dirPath, 'index.scss', reExportTemplateScss),
	]);
}

run().then(() => {
	process.exit(0);
});
