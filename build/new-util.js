import createReExport from './create-re-export';
import {utilJsTemplate} from './templates/util.js';
import reExportTemplateJs from './templates/re-export-js';
import path from 'path';
import fse from 'fs-extra';
import chalk from 'chalk';


const arg = process.argv[2];
const utilName = arg;

const run = async () => {
	const dirPath = path.resolve('./utils');
	if (utilName) {
		await fse.outputFile(`${dirPath}/${utilName}.js`, utilJsTemplate(utilName));
		console.log(chalk.green(`SUCCESS: Util - ${chalk.blue(utilName)} created`))
	} else {
		console.log(chalk.green(`SKIP: utilName is missing`));
	}
	await createReExport(dirPath, 'index.js', reExportTemplateJs, false);
}

run().then(() => {
	process.exit(0);
});
