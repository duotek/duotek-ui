export const vueTemplate = (name) => (
`<template>
	<div class="${name}">

	</div>
</template>

<script>
export default {
	name: '${name}',
	props: {},
	components: {},
	data() {
	 return {};
	},
	computed: {},
	methods: {},
	directives: {},
	filters: {},
	watch: {},
	mounted() {},
};
</script>
`);

export const indexJsTemplate = (name) => (
`import ${name} from './${name}.vue';

${name}.install = function(Vue) {
	Vue.component(${name}.name, ${name});
};

export default ${name};
`);

export const indexScssTemplate = (name) => (
`@use "./${name}";
`);
