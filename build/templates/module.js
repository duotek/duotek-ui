export const moduleJsTemplate = (name) => (
`
import check from '../../helpers/check';

class ${name} {
	constructor(Vue) {
		if (check.isNode) return;
		this.vm = Vue;
	}
}

export default ${name};
`);

export const indexJsTemplate = (name) => (
`import ${name} from './${name}.js';

${name}.install = function(Vue) {
	Vue.prototype.$${name.slice(0,1).toLowerCase()}${name.slice(1)} = new ${name}(Vue);
};

export default ${name};
`);

export const indexScssTemplate = (name) => (
	`@use "${name}";
`);
