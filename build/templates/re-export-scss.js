const _importTemplate = (name) => `@use './${name}';`

export const exportTemplate = (names) => (`// This file autogenerated width re-export-scss
${names.map(_importTemplate).join('\n')}
`
);

export default exportTemplate;
