import DAddon from './DAddon.vue';

DAddon.install = function(Vue) {
	Vue.component(DAddon.name, DAddon);
};

export default DAddon;
