import DAlert from './DAlert.vue';

DAlert.install = function(Vue) {
	Vue.component(DAlert.name, DAlert);
};

export default DAlert;
