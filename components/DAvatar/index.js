import DAvatar from './DAvatar.vue';

DAvatar.install = function(Vue) {
	Vue.component(DAvatar.name, DAvatar);
};

export default DAvatar;
