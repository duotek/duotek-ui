import DBadge from './DBadge.vue';

DBadge.install = function(Vue) {
	Vue.component(DBadge.name, DBadge);
};

export default DBadge;
