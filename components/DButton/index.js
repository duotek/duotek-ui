import DButton from './DButton.vue';

DButton.install = function(Vue, config = {}) {
	Object.keys(DButton.config).map((key) => {
		if (config[key] !== undefined) {
			DButton.config[key] = config[key];
		}
	})
	Vue.component(DButton.name, DButton);
};

export default DButton;
