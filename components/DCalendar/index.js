import DCalendar from './DCalendar.vue';

DCalendar.install = function(Vue) {
	Vue.component(DCalendar.name, DCalendar);
};

export default DCalendar;
