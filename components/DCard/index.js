import DCard from './DCard.vue';
import DCardRow from './DCardRow.vue';

DCard.install = function(Vue, config = {}) {
	Object.keys(DCard.config).map((key) => {
		if (config[key] !== undefined) {
			DCard.config[key] = config[key];
		}
	})
	Vue.component(DCard.name, DCard);
	Vue.component(DCardRow.name, DCardRow);
};

export default DCard;
