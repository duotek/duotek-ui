import DCells from './DCells.vue';
import DCell from './DCell.vue';

DCells.install = function(Vue) {
	Vue.component(DCells.name, DCells);
	Vue.component(DCell.name, DCell);
};

export default DCells;
