import { dropdownCore } from "./DDropdown-core";


const initDropdownAnchor = (id, directiveTarget, anchorTarget) => {

	const onClickTarget = (event) => {
		dropdownCore.emit(id, 'click', event);
	}

	dropdownCore.initAnchor(id, anchorTarget);
	anchorTarget.addEventListener('mousedown', onClickTarget);
	directiveTarget.$destroy = () => {
		dropdownCore.destroyAnchor(id);
		anchorTarget.removeEventListener('mousedown', onClickTarget);
	}
}

const checkExist = async (id) => { // Асинхронная функция - чтоб не вешать всю страницу при ошибке
	if (!dropdownCore.anchors[id]) return false;
	throw new Error(`id "${id}" already exist in page`)
}

const getAnchorTarget = function(target, binding) {
	if (binding.value && binding.value.closest) {
		return target.closest(binding.value.closest) || target;
	}
	return target;
}

const onBindDirective = async function(target, binding) {
	const id = binding.arg;
	await checkExist(id);
	const directiveTarget = target;
	const anchorTarget = getAnchorTarget(target, binding);
	initDropdownAnchor(id, directiveTarget, anchorTarget)
}

const onUnbindDirective = function(target) {
	target.$destroy && target.$destroy();
}

export default {
	inserted: onBindDirective,
	unbind: onUnbindDirective,
}
