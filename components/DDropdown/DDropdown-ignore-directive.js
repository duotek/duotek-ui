
const markEvent = (evt) => {
	const parent = evt.currentTarget;
	const el = evt.target;
	const closestProp = parent.props.closest;

	if (!closestProp || el.closest(closestProp)) {
		evt._dropdownIgnode = true;
	}
}

export default {
	bind: (el, binding) => {
		el.props = {
			closest: binding.value || undefined,
		}
		el.addEventListener('mousedown', markEvent);
	},
	unbind: (el) => {
		el.removeEventListener('mousedown', markEvent);
	},
}
