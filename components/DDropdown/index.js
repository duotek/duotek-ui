import DDropdown from './DDropdown.vue';
import DDropdownDirective from './DDropdown-directive';
import DDropdownIgnoreDirective from './DDropdown-ignore-directive';

DDropdown.install = function(Vue) {
	Vue.component(DDropdown.name, DDropdown);
	Vue.directive('dropdown', DDropdownDirective);
	Vue.directive('dropdown-ignore', DDropdownIgnoreDirective);
};

export default DDropdown;
