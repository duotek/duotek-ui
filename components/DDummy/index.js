import DDummy from './DDummy.vue';

DDummy.install = function(Vue) {
	Vue.component(DDummy.name, DDummy);
};

export default DDummy;
