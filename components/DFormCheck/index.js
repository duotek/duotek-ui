import DFormCheck from './DFormCheck.vue';

DFormCheck.install = function(Vue) {
	Vue.component(DFormCheck.name, DFormCheck);
};

export default DFormCheck;
