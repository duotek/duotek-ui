import DFormCode from './DFormCode.vue';

DFormCode.install = function(Vue) {
	Vue.component(DFormCode.name, DFormCode);
};

export default DFormCode;
