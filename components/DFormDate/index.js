import DFormDate from './DFormDate.vue';

DFormDate.install = function(Vue) {
	Vue.component(DFormDate.name, DFormDate);
};

export default DFormDate;
