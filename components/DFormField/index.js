import DFormField from './DFormField.vue';

DFormField.install = function(Vue) {
	Vue.component(DFormField.name, DFormField);
};

export default DFormField;
