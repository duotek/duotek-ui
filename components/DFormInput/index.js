import DFormInput from './DFormInput.vue';

DFormInput.install = function(Vue) {
	Vue.component(DFormInput.name, DFormInput);
};

export default DFormInput;
