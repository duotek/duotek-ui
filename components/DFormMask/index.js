import DFormMask from './DFormMask.vue';

DFormMask.install = function(Vue) {
	Vue.component(DFormMask.name, DFormMask);
};

export default DFormMask;
