import DFormPassword from './DFormPassword.vue';

DFormPassword.install = function(Vue) {
	Vue.component(DFormPassword.name, DFormPassword);
};

export default DFormPassword;
