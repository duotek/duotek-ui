import DFormRadio from './DFormRadio.vue';

DFormRadio.install = function(Vue) {
	Vue.component(DFormRadio.name, DFormRadio);
};

export default DFormRadio;
