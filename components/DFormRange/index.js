import DFormRange from './DFormRange.vue';

DFormRange.install = function(Vue) {
	Vue.component(DFormRange.name, DFormRange);
};

export default DFormRange;
