import DFormSelect from './DFormSelect.vue';

DFormSelect.install = function(Vue) {
	Vue.component(DFormSelect.name, DFormSelect);
};

export default DFormSelect;
