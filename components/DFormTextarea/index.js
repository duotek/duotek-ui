import DFormTextarea from './DFormTextarea.vue';

DFormTextarea.install = function(Vue) {
	Vue.component(DFormTextarea.name, DFormTextarea);
};

export default DFormTextarea;
