import DFormTime from './DFormTime.vue';

DFormTime.install = function(Vue) {
	Vue.component(DFormTime.name, DFormTime);
};

export default DFormTime;
