import DFormUploader from './DFormUploader.vue';

DFormUploader.install = function(Vue) {
	Vue.component(DFormUploader.name, DFormUploader);
};

export default DFormUploader;
