import DIcon from './DIcon.vue';

DIcon.install = function(Vue) {
	Vue.component(DIcon.name, DIcon);
};

export default DIcon;
