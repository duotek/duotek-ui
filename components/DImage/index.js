import DImage from './DImage.vue';

DImage.install = function(Vue) {
	Vue.component(DImage.name, DImage);
};

export default DImage;
