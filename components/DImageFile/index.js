import DImageFile from './DImageFile.vue';

DImageFile.install = function(Vue) {
	Vue.component(DImageFile.name, DImageFile);
};

export default DImageFile;
