import DLabel from './DLabel.vue';

DLabel.install = function(Vue) {
	Vue.component(DLabel.name, DLabel);
};

export default DLabel;
