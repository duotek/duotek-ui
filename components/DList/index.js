import DList from './DList.vue';
import DListItem from './DListItem.vue';

DList.install = function(Vue, config = {}) {
	Object.keys(DListItem.config).map((key) => {
		if (config[key] !== undefined) {
			DListItem.config[key] = config[key];
		}
	})
	Vue.component(DList.name, DList);
	Vue.component(DListItem.name, DListItem);
};

export default DList;
