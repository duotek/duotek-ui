import DMedia from './DMedia.vue';

DMedia.install = function(Vue) {
	Vue.component(DMedia.name, DMedia);
};

export default DMedia;
