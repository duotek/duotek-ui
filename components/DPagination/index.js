import DPagination from './DPagination.vue';

DPagination.install = function(Vue) {
	Vue.component(DPagination.name, DPagination);
};

export default DPagination;
