import DProgress from './DProgress.vue';

DProgress.install = function(Vue) {
	Vue.component(DProgress.name, DProgress);
};

export default DProgress;
