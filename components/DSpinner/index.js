import DSpinner from './DSpinner.vue';

/* istanbul ignore next */
DSpinner.install = function(Vue) {
	Vue.component(DSpinner.name, DSpinner);
};

export default DSpinner;
