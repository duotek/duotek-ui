import DTable from './DTable.vue';

DTable.install = function(Vue) {
	Vue.component(DTable.name, DTable);
};

export default DTable;
