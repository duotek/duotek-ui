import DTabs from './DTabs.vue';
import DTab from './DTab.vue';
import DTabGroup from './DTabGroup.vue';

DTabs.install = function(Vue) {
	Vue.component(DTabs.name, DTabs);
	Vue.component(DTab.name, DTab);
	Vue.component(DTabGroup.name, DTabGroup);
};

export default DTabs;
