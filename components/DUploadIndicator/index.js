import DUploadIndicator from './DUploadIndicator.vue';

DUploadIndicator.install = function(Vue) {
	Vue.component(DUploadIndicator.name, DUploadIndicator);
};

export default DUploadIndicator;
