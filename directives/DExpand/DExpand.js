import bindAll from '../../helpers/bindAll';

export default class DExpand {
	constructor() {
		bindAll.call(this);
	}

	checkOverflow(el) {
		const overflow = window.getComputedStyle(el).overflow;
		if (overflow === 'visible') {
			el.style.overflow = 'hidden';
		}
	}

	startOpen(el) {
		this.checkOverflow(el);
		el.style.display = null;
		el.style.maxHeight = 0;
		el.style.maxHeight = el.scrollHeight + 'px';
	}

	setOpen(el) {
		if (el.style.display !== 'none') return;
		el.style.display = null;
		el.style.maxHeight = null;
		el.style.overflow = null;
	}

	startClose(el) {
		if (el.style.display === 'none') return;
		this.checkOverflow(el);
		el.style.maxHeight = el.scrollHeight + 'px';
		setTimeout(() => {
			el.style.maxHeight = 0;
		}, 20);
	}

	setClose(el) {
		el.style.maxHeight = null;
		el.style.display = 'none';
		el.style.overflow = null;
	}

	updateElement(el, state) {
		const onTransition = () => {
			state ? this.setOpen(el) : this.setClose(el);
			el.removeEventListener('transitionend', onTransition, false);
		};
		el.addEventListener('transitionend', onTransition, false);
		state ? this.startOpen(el) : this.startClose(el);
	}

	inserted(el, {value = false} = {}) {
		el.classList.add('_d-expand');
		value ? this.setOpen(el) : this.setClose(el);
	}

	update(el, {value = false} = {}) {
		if (value !== null) {
			this.updateElement(el, value);
		}
	}
}
