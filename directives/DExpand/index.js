import DExpand from './DExpand';

DExpand.install = function (Vue) {
	const expand = new DExpand()
	Vue.directive('expand', {
		inserted: expand.inserted,
		update: expand.update,
	});
}

export default DExpand;
