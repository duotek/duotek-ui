const DFocus = {};

DFocus.install = function (Vue) {
	Vue.directive('focus', {
		inserted(el, {modifiers} = {}) {
			el.focus({preventScroll: modifiers.preventScroll})
		},
	});
}

export default DFocus;
