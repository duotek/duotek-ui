import check from '../../helpers/check';
import bindAll from '../../helpers/bindAll';
import { ResizeObserver as Polyfill } from '@juggle/resize-observer';

export default class {
	constructor(el, modifiers = {}) {
		bindAll.call(this);

		this.body = document.body;
		this.THUMB_MIN_SIZE = 20;
		this.elSource = el;
		this.elRoot = null;
		this.elOverlay = null;
		this.elTrackX = null;
		this.elTrackY = null;
		this.elThumbX = null;
		this.elThumbY = null;
		this.isBody = el === document.documentElement;
		this.resizeObserver = null;
		this.mutationObserver = null;
		this.mutationTextObserver = null;

		this.cache = {};

		this.modifiers = {
			showOnHover: modifiers.showOnHover,
			disableThumbHover: modifiers.disableThumbHover,
		};
		this.isDrag = false;
		this.isActiveThumbX = false;
		this.isActiveThumbY = false;
		this.overlayClientRect = null;
		this.property = {
			X: {
				scrollSize: 'scrollWidth',
				scrollSide: 'scrollLeft',
				clientSize: 'clientWidth',
				clientSide: 'clientX',
				rectSide: 'left',
			},
			Y: {
				scrollSize: 'scrollHeight',
				scrollSide: 'scrollTop',
				clientSize: 'clientHeight',
				clientSide: 'clientY',
				rectSide: 'top',
			},
		};

		this.setTemplate();
		this.addEventListeners();
		this.updateAll();
		this.elSource.classList.add('init');
		this.initObserver();
	}

	createDiv() {
		const div = document.createElement('div');
		arguments.length && div.classList.add(...arguments);
		return div;
	}

	setTemplate() {
		if (this.modifiers.showOnHover && !check.checkTouch()) {
			this.elSource.classList.add('--show-on-hover');
		}
		if (this.modifiers.disableThumbHover) {
			this.elSource.classList.add('--disable-thumb-hover');
		}

		this.elRoot = this.createDiv('DScrollBar');
		this.elOverlay = this.createDiv('DScrollBar__overlay');
		this.elTrackX = this.createDiv('DScrollBar__track-x');
		this.elTrackY = this.createDiv('DScrollBar__track-y');
		this.elThumbX = this.createDiv('DScrollBar__thumb-x');
		this.elThumbY = this.createDiv('DScrollBar__thumb-y');

		this.elTrackX.appendChild(this.elThumbX);
		this.elTrackY.appendChild(this.elThumbY);
		this.elOverlay.appendChild(this.elTrackX);
		this.elOverlay.appendChild(this.elTrackY);
		this.elRoot.appendChild(this.elOverlay);
		this.elSource.classList.add('_DScrollBar-source');

		if (this.isBody) {
			document.body.prepend(this.elRoot);
			document.body.classList.add('_DScrollBar-source');
		} else {
			this.elSource.prepend(this.elRoot);
		}

		const sourcePosition = getComputedStyle(this.elSource).position;
		if (['static', 'inherit'].includes(sourcePosition)) {
			this.elSource.style.position = 'relative';
		}
		if (this.isBody) {
			this.elRoot.style.position = 'fixed';
		}
	}

	mutationTextHandler() {
		this.setResizeObserve();
	}

	mutationHandler() {
		this.setResizeObserve();
	}

	resizeHandler() {
		this.updateAll();
	}

	setMutationTextObserve(textNode) {
		if (!this.mutationTextObserver) {
			this.mutationTextObserver = new MutationObserver(this.mutationTextHandler);
		}
		this.mutationTextObserver.observe(textNode, {
			characterData: true,
		});
	}

	setMutationObserve() {
		this.mutationObserver.observe(this.elSource, {
			childList: true,
			attributes: true,
		});
	}

	setResizeObserve() {
		this.resizeObserver.disconnect();
		this.resizeObserver.observe(this.elSource);
		this.mutationTextObserver && this.mutationTextObserver.disconnect();
		const source = this.isBody ? document.body : this.elSource;
		const blackListNodeNames = ['SCRIPT', '#comment'];
		for (const el of source.childNodes) {
			if (el.nodeName === '#text') {
				this.setMutationTextObserve(el);
			} else if (el !== this.elRoot && !blackListNodeNames.includes(el.nodeName)) {
				this.resizeObserver.observe(el);
			}
		}
	}

	initObserver() {
		const ResizeObserver = window.ResizeObserver || Polyfill;
		this.mutationObserver = new MutationObserver(this.mutationHandler);
		this.resizeObserver = new ResizeObserver(this.resizeHandler);
		this.setMutationObserve();
		this.setResizeObserve();
	}

	destroyObserver() {
		this.mutationObserver.disconnect();
		this.resizeObserver.disconnect();
	}

	calculatePositionOverlay() {
		const offsetTop = this.elSource.scrollTop - this.elRoot.offsetTop;
		const offsetLeft = this.elSource.scrollLeft - this.elRoot.offsetLeft;
		const translate = `translate(${offsetLeft}px, ${offsetTop}px)`;
		return (offsetTop || offsetLeft) ? translate : null;
	}

	calculateThumbSize(key) {
		const props = this.property[key];
		const rate = this.elSource[props.clientSize] / this.elSource[props.scrollSize];
		const size = Math.ceil(this.elOverlay[props.clientSize] * rate);
		return rate === 1 ? 0 : Math.max(size, this.THUMB_MIN_SIZE);
	}

	calculateThumbOffset(key) {
		const props = this.property[key];
		const trackSize = this.elOverlay[props.clientSize] - this.calculateThumbSize(key);
		const scrollSize = this.elSource[props.scrollSize] - this.elSource[props.clientSize];
		const currentScroll = this.elSource[props.scrollSide];
		const rate = scrollSize / trackSize;
		const thumbOffset = currentScroll / rate;
		return `translate${key}(${thumbOffset}px)`;
	}

	calculateScrollPosition(key, evt) {
		const props = this.property[key];
		const trackSize = this.elOverlay[props.clientSize] - this.calculateThumbSize(key);
		const scrollSize = this.elSource[props.scrollSize] - this.elSource[props.clientSize];
		const rate = scrollSize / trackSize;
		const evtClientSide = evt[props.clientSide];
		const overlayClientRectSide = this.overlayClientRect[props.rectSide];
		const thumbOffset = this.thumbOffset[props.rectSide];
		const delta = evtClientSide - overlayClientRectSide - thumbOffset;
		return rate * delta;
	}

	setStyleSafe(key, el, style, value) {
		if (this.cache[key] !== value) {
			el.style[style] = value;
			this.cache[key] = value;
		}
	}

	setSizeOverlay() {
		let height, width;
		if (this.isBody) {
			height = window.innerHeight;
			width = window.innerWidth;
		} else {
			const sourceClientRect = this.elSource.getBoundingClientRect();
			height = Math.abs(sourceClientRect.bottom - sourceClientRect.top);
			width = Math.abs(sourceClientRect.right - sourceClientRect.left);
		}
		this.setStyleSafe('elOverlay.height', this.elOverlay, 'height', height + 'px');
		this.setStyleSafe('elOverlay.width', this.elOverlay, 'width', width + 'px');
	}

	setPositionOverlay() {
		if (this.isBody) return;
		this.setStyleSafe('elRoot.transform', this.elRoot, 'transform', this.calculatePositionOverlay());
	}

	setSizeThumbX() {
		this.setStyleSafe('elThumbX.width', this.elThumbX, 'width', this.calculateThumbSize('X') + 'px');
	}

	setSizeThumbY() {
		this.setStyleSafe('elThumbY.height', this.elThumbY, 'height', this.calculateThumbSize('Y') + 'px');
	}

	setPositionThumbX() {
		this.setStyleSafe('elThumbX.transformX', this.elThumbX, 'transform', this.calculateThumbOffset('X'));
	}

	setPositionThumbY() {
		this.setStyleSafe('elThumbY.transformY', this.elThumbY, 'transform', this.calculateThumbOffset('Y'));
	}

	setSourceScrollX(evt) {
		this.elSource.scrollTo({left: this.calculateScrollPosition('X', evt)});
	}

	setSourceScrollY(evt) {
		this.elSource.scrollTo({top: this.calculateScrollPosition('Y', evt)});
	}

	setSizeThumb() {
		this.setSizeThumbY();
		this.setSizeThumbX();
	}

	updatePositionThumb() {
		this.setPositionThumbX();
		this.setPositionThumbY();
	}

	tryHideTrack(key) {
		let isHide = true;
		const style = getComputedStyle(this.elSource)
		const overflow = style[`overflow${key}`];
		if (['scroll', 'auto'].includes(overflow)) {
			const props = this.property[key];
			isHide = this.elSource[props.clientSize] >= this.elSource[props.scrollSize] - 1;
		}
		this.setStyleSafe(`elTrack${key}`, this[`elTrack${key}`], 'display', isHide ? 'none' : '');
		return isHide;
	}

	tryHide() {
		const hiddenX = this.tryHideTrack('X');
		const hiddenY = this.tryHideTrack('Y');
		return !hiddenX || !hiddenY;
	}

	updateAll() {
		this.tryHide() && requestAnimationFrame(() => {
			this.setPositionOverlay();
			this.setSizeOverlay();
			this.setSizeThumb();
			this.updatePositionThumb();
		});
	}

	onSourceScroll() {
		requestAnimationFrame(this.updatePositionThumb);
	}

	preventEvent(evt) {
		evt.stopImmediatePropagation();
		evt.stopPropagation();
		evt.preventDefault();
	}

	onDragStart(evt) {
		this.isActiveThumbX = evt.target === this.elThumbX;
		this.isActiveThumbY = evt.target === this.elThumbY;
		if (this.isActiveThumbX || this.isActiveThumbY) {
			this.preventEvent(evt);
			this.isDrag = true;
			this.thumbOffset = {
				left: evt.clientX - this.elThumbX.getBoundingClientRect().left,
				top: evt.clientY - this.elThumbY.getBoundingClientRect().top,
			};
			this.overlayClientRect = this.elOverlay.getBoundingClientRect();
			this.isActiveThumbX && this.elTrackX.classList.add('--active');
			this.isActiveThumbY && this.elTrackY.classList.add('--active');
			this.body.classList.add('_d-unselectable');
			this.addDragEventListeners();
		}
	}

	onDragEnd(evt) {
		this.isDrag = false;
		this.isActiveThumbX = false;
		this.isActiveThumbY = false;
		this.thumbOffset = null;
		this.overlayClientRect = null;
		this.body.classList.remove('_d-unselectable');
		this.elTrackX.classList.remove('--active');
		this.elTrackY.classList.remove('--active');
		this.removeDragEventListeners();
		this.preventEvent(evt);
	}

	onDrag(evt) {
		if (!this.isDrag) return;
		this.isActiveThumbX && this.setSourceScrollX(evt);
		this.isActiveThumbY && this.setSourceScrollY(evt);
	}

	onBodyClick(evt) {
		this.preventEvent(evt);
	}

	addDragEventListeners() {
		document.addEventListener('mousemove', this.onDrag);
		document.addEventListener('mouseup', this.onDragEnd);
		document.addEventListener('click', this.onBodyClick);
	}

	removeDragEventListeners() {
		document.removeEventListener('mousemove', this.onDrag);
		document.removeEventListener('mouseup', this.onDragEnd);
		document.removeEventListener('click', this.onBodyClick);
	}

	addEventListeners() {
		const source = this.isBody ? window : this.elSource;
		source.addEventListener('scroll', this.onSourceScroll, {passive: true});
		this.elTrackY.addEventListener('mousedown', this.onDragStart);
		this.elTrackX.addEventListener('mousedown', this.onDragStart);
	}

	removeEventListeners() {
		const source = this.isBody ? window : this.elSource;
		source.removeEventListener('scroll', this.onSourceScroll);
		this.elTrackY.removeEventListener('mousedown', this.onDragStart);
		this.elTrackX.removeEventListener('mousedown', this.onDragStart);
	}

	destroy() {
		this.removeEventListeners();
		this.destroyObserver();
		this.elRoot.remove();
	}
}
