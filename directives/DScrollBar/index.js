import DScrollBar from './DScrollBar';
import check from '../../helpers/check';

DScrollBar.install = function (Vue, options = {}) {
	const {bodyScrollBar} = options;
	if (check.isBrowser && !check.checkTouch() && bodyScrollBar) {
		document.documentElement.classList.add('__bodyScrollBarEnable');
		new DScrollBar(document.documentElement);
	}
	Vue.directive('scroll-bar', {
		inserted(el, {modifiers} = {}) {
			const isSupported = check.checkSupportedMutationObserver() && check.checkSupportedResizeObserver();
			if (isSupported) {
				el._dScrollBar = new DScrollBar(el, modifiers);
			}
		},
		unbind(el) {
			if (!el._dScrollBar) return;
			el._dScrollBar.destroy();
			delete el._dScrollBar;
		},
	});
}

export default DScrollBar;
