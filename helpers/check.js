export default {
	isBrowser: typeof window !== 'undefined' && typeof window.document !== 'undefined',
	isNode: typeof process !== 'undefined' && process.versions != null && process.versions.node != null,
	isIOs: typeof window !== 'undefined' && /iPad|iPhone|iPod/.test(navigator.userAgent) && !window.MSStream,
	checkTouch: () => window.matchMedia('(hover: none)').matches,
	checkSupportedMutationObserver: () => typeof window !== 'undefined' && Boolean(window.MutationObserver),
	checkSupportedResizeObserver: () => typeof window !== 'undefined' && Boolean(window.ResizeObserver),
}
