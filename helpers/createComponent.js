import {typeBool, typeSizes} from './propTypes';

export const checkBoolProp = (prop) => prop !== void (0) && prop !== false && prop !== null;
export const mergeProps = (context, options = {}) => {

	const _class = [];

	if (options.class) {
		if (Array.isArray(options.class)) {
			_class.push(...options.class);
		} else if (typeof options.class === 'object') {
			_class.push(options.class);
		}
	}

	if (context.data.class) {
		_class.push(context.data.class);
	}

	if (context.data.staticClass) {
		_class.push(context.data.staticClass);
	}

	if (context.data.attrs && context.data.attrs.class) {
		_class.push(context.data.attrs.class);
		delete context.data.attrs.class;
	}

	return {
		class: _class,
		style: [context.data.style, context.data.staticStyle],
		attrs: {
			...(options.attrs || {}),
			...(context.data.attrs || {}),
		},
		props: {
			...(options.props ? options.props : {}),
		},
		domProps: {
			...(options.domProps ? options.domProps : {}),
		},
		directives: context.data.directives,
		on: {
			...(options.on || {}),
			...context.listeners
		},
		...(options.nativeOn && {
			nativeOn: options.nativeOn
		}),
		ref: context.data.ref,
	};
};

export const gapPropsList = {
	gap: typeSizes,
	gapX: typeSizes,
	gapY: typeSizes,
	xxxlGap: typeSizes,
	xxlGap: typeSizes,
	xlGap: typeSizes,
	lGap: typeSizes,
	mGap: typeSizes,
	sGap: typeSizes,
	xsGap: typeSizes,
	xxsGap: typeSizes,
	xxxlGapX: typeSizes,
	xxlGapX: typeSizes,
	xlGapX: typeSizes,
	lGapX: typeSizes,
	mGapX: typeSizes,
	sGapX: typeSizes,
	xsGapX: typeSizes,
	xxsGapX: typeSizes,
	xxxlGapY: typeSizes,
	xxlGapY: typeSizes,
	xlGapY: typeSizes,
	lGapY: typeSizes,
	mGapY: typeSizes,
	sGapY: typeSizes,
	xsGapY: typeSizes,
	xxsGapY: typeSizes
};

export const spanPropsList = {
	span: typeSizes,
	xxxlSpan: typeSizes,
	xxlSpan: typeSizes,
	xlSpan: typeSizes,
	lSpan: typeSizes,
	mSpan: typeSizes,
	sSpan: typeSizes,
	xsSpan: typeSizes,
	xxsSpan: typeSizes,
}

// Ручное заполнение объекта значительно быстрее переборки значений в цикле, для рендера больших списков компонент
export const createGapClassList = (props) => {
	const classObject = {
		gap: (Boolean(props['gap']) || props['gap'] === 0) ?`_d--gap-x:${props['gap']} _d--gap-y:${props['gap']}` : '',
		xxxlGap: (Boolean(props['xxxlGap']) || props['xxxlGap'] === 0) ?`_d--xxxl:gap-x:${props['xxxlGap']} _d--xxxl:gap-y:${props['xxxlGap']}` : '',
		xxlGap: (Boolean(props['xxlGap']) || props['xxlGap'] === 0) ?`_d--xxl:gap-x:${props['xxlGap']} _d--xxl:gap-y:${props['xxlGap']}` : '',
		xlGap: (Boolean(props['xlGap']) || props['xlGap'] === 0) ?`_d--xl:gap-x:${props['xlGap']} _d--xl:gap-y:${props['xlGap']}` : '',
		lGap: (Boolean(props['lGap']) || props['lGap'] === 0) ?`_d--l:gap-x:${props['lGap']} _d--l:gap-y:${props['lGap']}` : '',
		mGap: (Boolean(props['mGap']) || props['mGap'] === 0) ?`_d--m:gap-x:${props['mGap']} _d--m:gap-y:${props['mGap']}` : '',
		sGap: (Boolean(props['sGap']) || props['sGap'] === 0) ?`_d--s:gap-x:${props['sGap']} _d--s:gap-y:${props['sGap']}` : '',
		xsGap: (Boolean(props['xsGap']) || props['xsGap'] === 0) ?`_d--xs:gap-x:${props['xsGap']} _d--xs:gap-y:${props['xsGap']}` : '',
		xxsGap: (Boolean(props['xxsGap']) || props['xxsGap'] === 0) ?`_d--xxs:gap-x:${props['xxsGap']} _d--xxs:gap-y:${props['xxsGap']}` : '',

		gapX: (Boolean(props['gapX']) || props['gapX'] === 0) ?`_d--gap-x:${props['gapX']}` : '',
		xxxlGapX: (Boolean(props['xxxlGapX']) || props['xxxlGapX'] === 0) ?`_d--xxxl:gap-x:${props['xxxlGapX']}` : '',
		xxlGapX: (Boolean(props['xxlGapX']) || props['xxlGapX'] === 0) ?`_d--xxl:gap-x:${props['xxlGapX']}` : '',
		xlGapX: (Boolean(props['xlGapX']) || props['xlGapX'] === 0) ?`_d--xl:gap-x:${props['xlGapX']}` : '',
		lGapX: (Boolean(props['lGapX']) || props['lGapX'] === 0) ?`_d--l:gap-x:${props['lGapX']}` : '',
		mGapX: (Boolean(props['mGapX']) || props['mGapX'] === 0) ?`_d--m:gap-x:${props['mGapX']}` : '',
		sGapX: (Boolean(props['sGapX']) || props['sGapX'] === 0) ?`_d--s:gap-x:${props['sGapX']}` : '',
		xsGapX: (Boolean(props['xsGapX']) || props['xsGapX'] === 0) ?`_d--xs:gap-x:${props['xsGapX']}` : '',
		xxsGapX: (Boolean(props['xxsGapX']) || props['xxsGapX'] === 0) ?`_d--xxs:gap-x:${props['xxsGapX']}` : '',

		gapY: (Boolean(props['gapY']) || props['gapY'] === 0) ?`_d--gap-y:${props['gapY']}` : '',
		xxxlGapY: (Boolean(props['xxxlGapY']) || props['xxxlGapY'] === 0) ?`_d--xxxl:gap-y:${props['xxxlGapY']}` : '',
		xxlGapY: (Boolean(props['xxlGapY']) || props['xxlGapY'] === 0) ?`_d--xxl:gap-y:${props['xxlGapY']}` : '',
		xlGapY: (Boolean(props['xlGapY']) || props['xlGapY'] === 0) ?`_d--xl:gap-y:${props['xlGapY']}` : '',
		lGapY: (Boolean(props['lGapY']) || props['lGapY'] === 0) ?`_d--l:gap-y:${props['lGapY']}` : '',
		mGapY: (Boolean(props['mGapY']) || props['mGapY'] === 0) ?`_d--m:gap-y:${props['mGapY']}` : '',
		sGapY: (Boolean(props['sGapY']) || props['sGapY'] === 0) ?`_d--s:gap-y:${props['sGapY']}` : '',
		xsGapY: (Boolean(props['xsGapY']) || props['xsGapY'] === 0) ?`_d--xs:gap-y:${props['xsGapY']}` : '',
		xxsGapY: (Boolean(props['xxsGapY']) || props['xxsGapY'] === 0) ?`_d--xxs:gap-y:${props['xxsGapY']}` : '',
	}
	return Object.values(classObject).filter(i => i);
};

export const createSpanClassList = (props) => {
	const classObject = {
		span: (Boolean(props['span']) || props['span'] === 0) ? `_d--span:${props['span']}` : '',
		xxxlSpan: (Boolean(props['xxxlSpan']) || props['xxxlSpan'] === 0) ? `_d--xxxl:span:${props['xxxlSpan']}` : '',
		xxlSpan: (Boolean(props['xxlSpan']) || props['xxlSpan'] === 0) ? `_d--xxl:span:${props['xxlSpan']}` : '',
		xlSpan: (Boolean(props['xlSpan']) || props['xlSpan'] === 0) ? `_d--xl:span:${props['xlSpan']}` : '',
		lSpan: (Boolean(props['lSpan']) || props['lSpan'] === 0) ? `_d--l:span:${props['lSpan']}` : '',
		mSpan: (Boolean(props['mSpan']) || props['mSpan'] === 0) ? `_d--m:span:${props['mSpan']}` : '',
		sSpan: (Boolean(props['sSpan']) || props['sSpan'] === 0) ? `_d--s:span:${props['sSpan']}` : '',
		xsSpan: (Boolean(props['xsSpan']) || props['xsSpan'] === 0) ? `_d--xs:span:${props['xsSpan']}` : '',
		xxsSpan: (Boolean(props['xxsSpan']) || props['xxsSpan'] === 0) ? `_d--xxs:span:${props['xxsSpan']}` : '',
	}
	return Object.values(classObject).filter(i => i);
};
