class DisableOutline {
	constructor() {
		this.body = null;
		this.onBodyClick = this.onBodyClick.bind(this);
		this.onKeyDown = this.onKeyDown.bind(this);
		this.isLockClick = false;
	}
	setBody() {
		this.body = document.querySelector('body');
		this.onBodyClick();
	}
	onBodyClick() {
		if (this.isLockClick) return;
		this.body.classList.add('disableOutline');
	}
	onKeyDown(event) {
		const isTab = event.code === 'Tab';
		if (isTab) {
			this.body.classList.remove('disableOutline');
		}
		this.lockClick();
	}
	lockClick() {
		this.isLockClick = true;
		setTimeout(() => {
			this.isLockClick = false;
		})
	}
	init() {
		if (process && process.server) return false;
		try {
			this.setBody();
			this.body.addEventListener('click', this.onBodyClick);
			window.addEventListener('keydown', this.onKeyDown);
			window.addEventListener('keyup', this.onKeyDown);
		} catch (e) {
			console.log(e);
		}
	}
	destroy() {
		if (process && process.server) return false;
		try {
			this.body.removeEventListener('click', this.onBodyClick);
			window.removeEventListener('keydown', this.onKeyDown);
			window.removeEventListener('keyup', this.onKeyDown);
		} catch (e) {
			console.log(e);
		}
	}
}

export default new DisableOutline();
