const D_SIZES = [
	'xxxl',
	'xxl',
	'xl',
	'l',
	'm',
	's',
	'xs',
	'xxs',
]

export const typeBool = {
	_type: Boolean,
	default: false,
}

export const typeSizes = {
	_type: [String, Number],
	variants: D_SIZES,
	default: '',
}


