import Vue from 'vue';
import disableOutline from './helpers/disableOutline';
import components from './components';
import modules from './modules';
import directives from './directives';
import utils from './utils';
import plugins from './plugins';

const install = function(Vue = Vue, options = {}) {
	Object.keys(components).forEach((key) => {
		if (components[key].install) {
			Vue.use(components[key], options[key]);
		}
	});
	Object.keys(modules).forEach((key) => {
		if (modules[key].install) {
			Vue.use(modules[key]);
		}
	});
	Object.keys(directives).forEach((key) => {
		if (directives[key].install) {
			Vue.use(directives[key], options[key]);
		}
	});
	Object.keys(plugins).forEach((key) => {
		if (plugins[key].install) {
			Vue.use(plugins[key], options[key]);
		}
	});
	Vue.prototype.$dUtils = utils;
	disableOutline.init();
};

export default {
	install,
};
