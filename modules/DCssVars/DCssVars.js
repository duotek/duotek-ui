
import check from '../../helpers/check';
import bindAll from '../../helpers/bindAll';

class DCssVars {
	constructor() {
		if (check.isNode) return;
		bindAll.call(this);
		this.setVhVar();
		window.addEventListener('resize', this.setVhVar);
	}

	setVhVar() {
		this.vh = window.innerHeight * 0.01;
		document.documentElement.style.setProperty('--vh', `${this.vh}px`);
		document.documentElement.style.setProperty('--100vh', `${this.vh * 100}px`);
	}
}

export default DCssVars;
