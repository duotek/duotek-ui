import DCssVars from './DCssVars.js';

DCssVars.install = function(Vue) {
	Vue.prototype.$dCssVars = new DCssVars(Vue);
};

export default DCssVars;
