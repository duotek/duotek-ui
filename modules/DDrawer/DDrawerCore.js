import { DialogsWrapper, create as dialogCreate} from '../../plugins/modalDialog';
import DDrawerContainer from './DDrawerContainer.vue';

class DDrawerCore {
	constructor(Vue) {
		this.vm = Vue;
		this.ROOT_NAME = 'DDrawerRoot';
		this.activeDrawer = {
			path: '',
			instance: null,
		};
		this._events = {};
		this.rootComponent = this.getRootComponent();
		this.rootInstance = null;
	}

	getRootComponent() {
		return {
			extends: DialogsWrapper,
			name: this.ROOT_NAME,
			props: {
				name: {
					type: String,
					default: this.ROOT_NAME,
				},
			},
			methods: {
				callbackCreated: (instance) => {
					this.rootInstance = instance;
				},
				callbackDestroy: (instance) => {
					if (this.rootInstance === instance) {
						this.rootInstance = null;
					}
				},
			},
		};
	}

	createDialog(path, props) {
		let animationForce = false;
		let anchor = null;
		let rootClass = '';

		if (this.activeDrawer.instance) {
			animationForce = true;
			this.activeDrawer.instance.$close();
		}

		if (props.anchor) {
			anchor = props.anchor;
			delete props.anchor
		}

		if (props.rootClass) {
			rootClass = props.rootClass;
			delete props.rootClass
		}

		return dialogCreate({
			wrapper: this.rootInstance,
			component: DDrawerContainer,
			props: ['drawerPath', 'drawerProps', 'anchor', 'animationForce', 'rootClass'],
		})(path, props, anchor, animationForce, rootClass);
	}

	load(path = '', props = {}) {
		const promise = this.createDialog(path, props);
		let _cashInstance = null;
		promise.getInstance && promise.getInstance().then(instance => {
			this.activeDrawer = {instance, path};
			_cashInstance = instance;
		});

		promise.then(() => {
			if (_cashInstance === this.activeDrawer.instance) {
				this.activeDrawer = {instance: null, path: ''};
			}
		});
		return promise;
	}

	on(eventName, handler) {
		if (!this._events[eventName] || !Array.isArray(this._events[eventName])) {
			this._events[eventName] = [];
		}

		this._events[eventName].push(handler);
	}

	off(eventName, handler) {
		if (!eventName) {
			this._events = {};
		} else if (!handler) {
			delete this._events[eventName];
		} else {
			let handlers = this._events && this._events[eventName];
			if (!handlers) return;
			const indexHandler = this._events[eventName].indexOf(handler);
			if (indexHandler === -1) return;
			this._events[eventName].splice(indexHandler, 1);
		}
	}

	emit(eventName, ...args) {
		if (!this._events[eventName]) return;

		this._events[eventName].forEach(handler => {
			handler.apply(this, args);
		});
	}

}

export default DDrawerCore;
