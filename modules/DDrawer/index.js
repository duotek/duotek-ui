import DDrawerCore from './DDrawerCore.js';

DDrawerCore.install = function(Vue) {
	const dDrawer = new DDrawerCore(Vue);
	Vue.prototype.$dDrawer = dDrawer;
	Vue.component('d-drawer-root', dDrawer.rootComponent);
};

export default DDrawerCore;
