import {DialogsWrapper, create as dialogCreate} from '../../plugins/modalDialog';
import DModalContainer from './DModalContainer';

class DModalCore {
	constructor(Vue) {
		this.vm = Vue;
		this.ROOT_NAME = 'DModalRoot';
		this.rootComponent = this.getRootComponent();
		this.rootInstance = null;
	}

	getRootComponent() {
		return {
			extends: DialogsWrapper,
			name: this.ROOT_NAME,
			props: {
				name: {
					type: String,
					default: this.ROOT_NAME,
				},
			},
			methods: {
				callbackCreated: (instance) => {
					this.rootInstance = instance;
				},
				callbackDestroy: (instance) => {
					if (this.rootInstance === instance) {
						this.rootInstance = null;
					}
				},
			},
		};
	}

	createDialog(path, props) {
		return dialogCreate({
			wrapper: this.rootInstance,
			component: DModalContainer,
			props: ['modalPath', 'modalProps'],
		})(path, props);
	}

	load(path = '', props = {}) {
		return this.createDialog(path, props);
	}
}

export default DModalCore;
