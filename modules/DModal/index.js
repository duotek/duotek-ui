import DModalCore from './DModalCore.js';

DModalCore.install = (Vue) => {
	const dModal = new DModalCore(Vue);
	Vue.prototype.$dModal = dModal;
	Vue.component('d-modal-root', dModal.rootComponent);
};

export default DModalCore;
