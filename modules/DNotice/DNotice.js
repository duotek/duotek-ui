import {createToastInterface, TYPE} from 'vue-toastification';
import DNoticeComponent from './DNotice.vue';
import check from '../../helpers/check';

class DNotice {
	constructor(Vue, options = {}) {
		if (check.isNode) return;
		this.vm = Vue;
		this.notice = null;
		this.options = {
			timeout: 3000,
			position: 'bottom-left',
			closeOnClick: false,
			maxToasts: 5,
			icon: false,
			closeButton: false,
			...options,
		};
		this.createNotice();
	}

	createNotice() {
		this.notice = createToastInterface(this.options, this.vm);
	}

	showNotice(type, content, options) {
		return this.notice({
			type,
			component: DNoticeComponent,
			props: {
				content,
				type
			}
		});
	}

	info(content = '', options = {}) {
		return this.showNotice(TYPE.INFO, content, options);
	}

	success(content = '', options = {}) {
		return this.showNotice(TYPE.SUCCESS, content, options);
	}

	warning(content = '', options = {}) {
		return this.showNotice(TYPE.WARNING, content, options);
	}

	error(content = '', options = {}) {
		return this.showNotice(TYPE.ERROR, content, options);
	}

	clearAll() {
		return this.notice.clear();
	}
}

export default DNotice;
