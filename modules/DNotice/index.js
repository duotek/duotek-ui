import DNotice from './DNotice.js';

DNotice.install = function(Vue) {
	Vue.prototype.$dNotice = new DNotice(Vue);
};

export default DNotice;
