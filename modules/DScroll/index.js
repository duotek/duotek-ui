import DScroll from './DScroll.js';

DScroll.install = function(Vue) {
	Vue.prototype.$dScroll = new DScroll(Vue);
};

export default DScroll;
