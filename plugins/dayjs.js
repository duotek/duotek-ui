import * as dayjs from 'dayjs';
import localeData from 'dayjs/plugin/localeData';
import isBetween from 'dayjs/plugin/isBetween';
import weekday from 'dayjs/plugin/weekday';
import customParseFormat from 'dayjs/plugin/customParseFormat';
import 'dayjs/locale/ru';

export default {
	install(Vue) {
		dayjs.extend(localeData);
		dayjs.extend(weekday);
		dayjs.extend(isBetween);
		dayjs.extend(customParseFormat);
		dayjs.locale('ru');

		Vue.prototype.$dayjs = dayjs;
	}
};
