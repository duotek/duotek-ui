// Вызывает функцию если она не вызывается установленное время
export default (func, time) => {
	let timeoutCall;
	return function () {
		const context = this;
		const args = arguments;
		clearTimeout(timeoutCall);
		timeoutCall = setTimeout(() => {
			func.apply(context, args);
		}, time)
	}
}
