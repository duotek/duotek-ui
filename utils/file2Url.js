export default async function file2Url(file) {
	if (!file || !file.type || !file.type.includes('image')) {
		throw new Error('Формат файла не поддерживается');
	}
	const reader = new FileReader();
	return new Promise((resolve) => {
		reader.onload = (event) => {
			resolve(event.target.result);
		};
		reader.readAsDataURL(file);
	});
}
