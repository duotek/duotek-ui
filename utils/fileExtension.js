export default function fileExtension(filename = '') {
	return filename.substring(filename.lastIndexOf('.') + 1, filename.length) || '?';
}
