const isObject = val => val && typeof val === 'object';

const mergeDeep = function (...objects) {
	return objects.reduce((prev, obj) => {
		(obj !== null) && Object.keys(obj).forEach(key => {
			const pVal = prev[key];
			const oVal = obj[key];

			if (Array.isArray(pVal) && Array.isArray(oVal)) {
				prev[key] = pVal.concat(...oVal);
			}
			else if (isObject(pVal) && isObject(oVal)) {
				prev[key] = mergeDeep(pVal, oVal);
			}
			else {
				prev[key] = oVal === undefined ? pVal : oVal;
			}
		});

		return prev;
	}, {});
}

export default mergeDeep
