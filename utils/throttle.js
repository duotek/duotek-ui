// Вызывает функцию не чаще установленного времени
export default (func, wait) => {
	let lastCall;
	let timeoutCall;
	return function () {
		const context = this;
		const args = arguments;
		const now = performance.now();
		if (!lastCall || (now - lastCall) >= wait) {
			clearTimeout(timeoutCall);
			func.apply(context, args);
			lastCall = now;
		} else {
			clearTimeout(timeoutCall);
			timeoutCall = setTimeout(() => {
				func.apply(context, args);
				lastCall = now;
			}, wait - (now - lastCall))
		}
	}
}
